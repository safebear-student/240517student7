package com.safebear.app;

/**
 * Created by Admin on 24/05/2017.
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Test01_Login.class,
})
public class TestSuite{
}
